package org.igniterealtime.restclient.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

/**
 * The Class MessageEntity.
 */
@XmlRootElement(name = "message")
@XmlType(propOrder = { "body", "username"})
public class MessageEntity {

	/** The body. */
	private String body;

	/** The username. */
	private String username;

	/**
	 * Instantiates a new message entity.
	 */
	public MessageEntity() {
	}

	public MessageEntity(String body, String username) {
		this.body = body;
		this.username = username;
	}

	public MessageEntity(String body) {
		this.body = body;
	}

	/**
	 * Gets the body.
	 *
	 * @return the body
	 */
	@XmlElement
	public String getBody() {
		return body;
	}

	/**
	 * Sets the body.
	 *
	 * @param body
	 *            the new body
	 */
	public void setBody(String body) {
		this.body = body;
	}

	/**
	 * Gets the username.
	 *
	 * @return the username
	 */
	@XmlElement
	public String getUsername() {
		return username;
	}

	/**
	 * Sets the username.
	 *
	 * @param username
	 *            the new username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

}
